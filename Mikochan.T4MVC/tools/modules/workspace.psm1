. (Join-Path $PSScriptRoot "..\module_init.ps1") @args

function Install-T4MVC {
    param (
        [Parameter(Mandatory=$false)][string]$ProjectSubPath,
        [Parameter(Mandatory=$false)][string]$TemplateType
    )
    Write-NoticeUI "Checking prerequisites..."

    $project = Get-Project
    $packageRefName = "T4MVCExtensions"
    $packageRefVersionCheck = "3.17.5"
    $packageRef = Get-Package $packageRefName -ProjectName $project.Name
    if ((-not $packageRef) -or (-not $packageRef.Version.ToString().StartsWith($packageRefVersionCheck))) {
        Write-Error "Please install $($packageRefName) v$($packageRefVersionCheck) first."
        return
    }

    Write-NoticeUI "Installing T4MVC template and settings..."

    $projectPath = Split-Path $project.FileName
    $ProjectSubPath = if (-not $ProjectSubPath) { "T4MVC" } else { $ProjectSubPath }
    
    $contentPath = Join-Path $ToolsPath "content"
    $settingsPath = Join-Path $contentPath $TemplateType
    if (-not(Test-Path -Path $settingsPath)) {
        Write-Error "Could not find tools path for template type '$($TemplateType)'."
        return
    }

    $t4MvcTemplate = Join-Path $contentPath "T4MVC.tt"
    $hooksFile = Join-Path $settingsPath "T4MVC.tt.hooks.t4"
    $settingsFile = Join-Path $settingsPath "T4MVC.tt.settings.xml"
    $filesToCopy = @{
        $t4MvcTemplate = "T4MVC.tt";
        $hooksFile = "T4MVC.tt.hooks.t4";
        $settingsFile = "T4MVC.tt.settings.xml";
    }

    $targetPath = Join-Path $projectPath $ProjectSubPath
    New-Item -ItemType Directory -Path $targetPath -Force | Out-Null
    $filesToCopy.GetEnumerator() | %{
        $copyTarget = Join-Path $targetPath $_.Value
        $result = Copy-Item -Path $_.Name -Destination $copyTarget -Force -PassThru -ErrorAction Inquire
        if ($result) {
            Write-Host "Copied '$($_.Name)'"
            $project.ProjectItems.AddFromFile($result.FullName) | Out-Null
        } else {
            Write-WarnUI "Skipped '$($_.Name)'"
        }
    }

    Write-NoticeUI "Install finished."
}

Export-ModuleMember -Function Install-T4MVC
