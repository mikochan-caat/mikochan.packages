. (Join-Path $PSScriptRoot "..\module_init.ps1") @args

function Install-Linq2DbTemplates {
    param (
        [Parameter(Mandatory=$false)][string]$ProjectSubPath
    )
    Write-NoticeUI "Installing Linq2DB templates..."

    $project = Get-Project
    $projectPath = Split-Path $project.FileName
    $ProjectSubPath = if (-not $ProjectSubPath) { "Linq2DB" } else { $ProjectSubPath }

    $targetPath = Join-Path $projectPath $ProjectSubPath
    $templateTargetPath = Join-Path $targetPath "$($project.Name).tt"
    $contentPath = Join-Path $ToolsPath "content"
    $includePath = Join-Path $contentPath "include"

    New-Item -ItemType Directory -Path $targetPath -Force | Out-Null    
    Copy-Item -Path (Join-Path $contentPath "Template.template") -Destination $templateTargetPath -Force | Out-Null
    $project.ProjectItems.AddFromFile($templateTargetPath) | Out-Null
    Write-Host "Copied '$(Split-Path $templateTargetPath -Leaf)'"

    $childItems = New-Object System.Collections.Generic.List[string]
    $parentItem = $null
    Get-ChildItem -Path $includePath | Copy-Item -Destination $targetPath -Recurse -Container -Force -PassThru | %{
        Write-Host "Copied '$($_.Name)'"
        if (-not($_.Name -eq "LinqToDB.ttinclude")) {
            $childItems.Add($_.FullName) | Out-Null
        } else {
            $parentItem = $project.ProjectItems.AddFromFile($_.FullName)
        }
    }
    if ($parentItem) {
        $childItems | %{
            $parentItem.ProjectItems.AddFromFile($_) | Out-Null
        }
    }

    Write-NoticeUI "Install finished."
}

Export-ModuleMember -Function Install-Linq2DbTemplates
