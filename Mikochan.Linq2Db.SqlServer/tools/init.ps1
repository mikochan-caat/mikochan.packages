#Requires -version 3.0
param($InstallPath, $ToolsPath, $Package)
$ErrorActionPreference = 'Stop'

. (Join-Path $PSScriptRoot "module_importer.ps1")

Register-Module "workspace.psm1"

# Copy the lib files to tools (if they do not exist or are older)
$solutionRoot = Split-Path $dte.Solution.FileName
$toolsLibPath = Join-Path $ToolsPath "lib"
$targetPath = Join-Path $solutionRoot ".tools\linq2db.t4models"

. (Join-Path $PSScriptRoot "robocopy.ps1")
Copy-DirectoryMirroredAsync -Source $toolsLibPath -Destination $targetPath -Filter "*.dll"
