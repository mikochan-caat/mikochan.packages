﻿$ErrorActionPreference = "Stop"

function Wait-Pause {
    echo ""
    echo "Press any key to continue..."
    cmd.exe /c "pause" | Out-Null
}

function Wait-Exit {
    Wait-Pause
    exit
}

function Write-Notice {
    param (
        [Parameter(Mandatory=$true)][string]$Text
    )
    Write-Host $Text -ForegroundColor Green
}

function Write-NoticeUI {
    param (
        [Parameter(Mandatory=$true)][string]$Text
    )
    Write-Host $Text -ForegroundColor DarkGreen
}

function Write-Warn {
    param (
        [Parameter(Mandatory=$true)][string]$Text
    )
    Write-Host $Text -ForegroundColor Yellow
}

function Write-WarnUI {
    param (
        [Parameter(Mandatory=$true)][string]$Text
    )
    Write-Host $Text -ForegroundColor DarkYellow
}

function Write-Critical {
    param (
        [Parameter(Mandatory=$true)][string]$Text
    )
    Write-Host $Text -ForegroundColor Red
}

function Add-EnvPath {
    param(
        [Parameter(Mandatory=$true)][string[]]$Paths
    )
    $appendedPath = "$($env:PATH);$($Paths -join ';')"
    $env:PATH = ($appendedPath -split ';' | Sort-Object -Unique) -join ';'
}

function Import-PathLibraries {
    param(
        [Parameter(Mandatory=$true)][string]$Path
    )
    Get-ChildItem -Path $Path -Filter "*.dll" | %{
        [Reflection.Assembly]::LoadFrom($_.FullName) | Out-Null
    }
}

function Send-EventMessage {
    param(
        [Parameter(Mandatory=$true)][string]$EventName,
        [Parameter(Mandatory=$true)][string]$Message
    )
    New-Event -SourceIdentifier $EventName -MessageData $Message | Out-Null
}

function Get-PackageArchitecture {
    param(
        [Parameter(Mandatory=$true)][string]$ToolsPath, 
        [switch]$UseIsolation
    )

    if (-not $env:MIKOCHAN_PACKAGE_ARCHITECTURE) {
        if ($UseIsolation.IsPresent) {
            $architectureJob = Start-Job -ScriptBlock {
                param($params)
                $ErrorActionPreference = "Stop"
                . (Join-Path $params.ToolsPath "common_init.ps1")
    
                Import-PathLibraries $params.ToolsPath
                return [Mikochan.Packages.Core.PackageArchitecture]::GetPackageString()
            } -ArgumentList @{
                ToolsPath = $ToolsPath
            }
            Wait-Job $architectureJob | Out-Null
            $env:MIKOCHAN_PACKAGE_ARCHITECTURE = Receive-Job $architectureJob
            $architectureJob | Stop-Job -PassThru | Remove-Job
        } else {
            $env:MIKOCHAN_PACKAGE_ARCHITECTURE = [Mikochan.Packages.Core.PackageArchitecture]::GetPackageString()
        }
    }
    
    return $env:MIKOCHAN_PACKAGE_ARCHITECTURE
}

function Find-PackagePath {
    param([Parameter(Mandatory=$true)][string]$PackageId)

    $package = Get-Package $PackageId | Sort-Object -Property Version -Descending | Select-Object -First 1
    if (-not $package) {
        return $null
    }

    return "packages\$($PackageId).$($package.Version.ToString())\"
}

function Invoke-WithPathEnvironment {
    param(
        [Parameter(Mandatory=$true)][scriptblock]$Action,
        [Parameter(Mandatory=$true)][string[]]$Paths
    )

    $savedEnvPath = $env:PATH
    try {
        Add-EnvPath -Paths $Paths
        & $Action
    } finally {
        $env:PATH = $savedEnvPath
    }
}

function Add-ArrayItems {
    param(
        [Parameter(Mandatory=$true)]
        [AllowEmptyCollection()]
        [Collections.ArrayList]$Array,
        [Parameter(Mandatory=$false)]
        [object[]]$Items
    )

    if ($Items.Length -gt 0) {
        $Array.AddRange($Items) | Out-Null
    }
}
