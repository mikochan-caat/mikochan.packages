﻿function Copy-DirectoryMirroredAsync {
    param (
        [Parameter(Mandatory=$true)][string]$Source,
        [Parameter(Mandatory=$true)][string]$Destination,
        [Parameter(Mandatory=$false)][string]$Filter
    )

    if (-not $Filter) {
        $Filter = "*.*"
    }

    Start-Process -FilePath "robocopy.exe" `
        -ArgumentList "`"$($Source)`"", "`"$($Destination)`"", "$($Filter) /MIR /XO" `
        -WindowStyle Hidden | Out-Null
}
