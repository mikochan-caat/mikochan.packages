﻿$CSharpProvider = [CodeDom.Compiler.CodeDomProvider]::CreateProvider("C#")
$UnicodeCharsRegex = New-Object "System.Text.RegularExpressions.Regex" `
    -ArgumentList @("[^\p{Ll}\p{Lu}\p{Lt}\p{Lo}\p{Nd}\p{Nl}\p{Mn}\p{Mc}\p{Cf}\p{Pc}\p{Lm}]")
$IdentifierCleanerRegex = New-Object "System.Text.RegularExpressions.Regex" `
    -ArgumentList @("(^\s*(?<firstChar>[^\s])|(?<whitespace>\s))")

function ConvertTo-ValidCsIdentifier {
    param (
        [Parameter(Mandatory=$true)][string]$Identifier
    )

    if (-not $CSharpProvider.IsValidIdentifier($Identifier)) {
        $Identifier = $UnicodeCharsRegex.Replace($Identifier, "")
    }
    return $IdentifierCleanerRegex.Replace($Identifier, [Text.RegularExpressions.MatchEvaluator]{
        param($match)

        if ($match.Groups["whitespace"].Success) {
            return ""
        }

        $firstCharMatch = $match.Groups["firstChar"];
        if ($firstCharMatch.Success -and -not [Char]::IsLetter($firstCharMatch.Value, 0)) {
            return "_$($firstCharMatch.Value)"
        }
        return $match.Value
    })
}

function ConvertTo-PathCsNamespace {
    param (
        [Parameter(Mandatory=$true)]
        [AllowEmptyString()]
        [string]$Path
    )
    
    if (-not $Path) {
        return $Path
    }

    $splittedPath = (Split-Path -Path $Path -NoQualifier) -split "\\" | ?{ -not [String]::IsNullOrWhiteSpace($_) }
    $convertedPath = $splittedPath | %{ ConvertTo-ValidCsIdentifier -Identifier $_ } | ?{ -not [String]::IsNullOrWhiteSpace($_) }
    return $convertedPath -join "."
}
