﻿. (Join-Path $PSScriptRoot "common_init.ps1")

$SolutionNode = Get-Interface $dte.Solution ([EnvDTE80.Solution2])
$SolutionDir = Split-Path $SolutionNode.FileName
$InstallPath = $args[0]
$ToolsPath = $args[1]
$Package = $args[2]
