﻿function Register-Module {
    param (
        [Parameter(Mandatory=$true)][string]$ModulePath
    )
    Import-Module (Join-Path "$($ToolsPath)\modules" $ModulePath) `
        -Force `
        -ArgumentList $InstallPath, $ToolsPath, $Package
}
