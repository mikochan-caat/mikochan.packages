. (Join-Path $PSScriptRoot "..\module_init.ps1") @args
Import-Module -Name (Join-Path $ToolsPath "csharplang.psm1") -Force

$QuickTypeInstallDir = Join-Path $SolutionDir ".tools\quicktype.powershell"
$VersioningFile = Join-Path $QuickTypeInstallDir "versioning"
$CurrentPackageVersion = $Package.Version.ToString()

function Install-QuickType {
    param(
        [switch]$Force
    )
    
    $project = Get-Project
    $projectDir = Split-Path $project.FullName

    if ((-not $Force.IsPresent) -and (Test-QuickTypeInstallation)) {
        $installedVersion = Get-Content -Path $VersioningFile -First 1
        if ($installedVersion -eq $CurrentPackageVersion) {
            Write-Error "A QuickType installation has been detected, use -Force to overwrite the existing installation."
        }
    }
    New-Item -Path $QuickTypeInstallDir -ItemType Directory -Force | Out-Null
    $CurrentPackageVersion | Out-File -FilePath $VersioningFile -Force

    $jobLogEventName = "QuickType.Installation"
    try {
        $installJob = Start-Job -ScriptBlock {
            param($params)
            $ErrorActionPreference = "Stop"
            . (Join-Path $params.ToolsPath "common_init.ps1")
    
            Import-PathLibraries -Path $params.ToolsPath
            Register-EngineEvent -SourceIdentifier $params.JobLogEventName -Forward
            Send-EventMessage -EventName $params.JobLogEventName -Message "Installing QuickType NPM package..."
        
            $quickTypeToolsFolder = Join-Path $params.ToolsPath "quicktype"
            $installationArchivePath = Join-Path $quickTypeToolsFolder "node_modules.zip"
            [Mikochan.Packages.Core.ZipArchiveHandler]::GetExtractionEnumerator( 
                $installationArchivePath, 
                $params.QuickTypeInstallDir) | %{
                if ($_ % 10 -eq 0 -or $_ -eq 1) {
                    Send-EventMessage -EventName $params.JobLogEventName -Message "[Install]: Extracted $($_)%"
                }
            }
            Send-EventMessage -EventName $params.JobLogEventName -Message "Copying executables..."
            $quickTypeExecutablePath = Join-Path $quickTypeToolsFolder "quicktype.cmd"
            Copy-Item -Path $quickTypeExecutablePath -Destination $params.QuickTypeInstallDir -Force | Out-Null
            
            Send-EventMessage -EventName $params.JobLogEventName -Message "Installation successful."
        } -ArgumentList @{
            ToolsPath = $ToolsPath;
            JobLogEventName = $jobLogEventName;
            QuickTypeInstallDir = $QuickTypeInstallDir;
        }
    
        $progressListener = Register-EngineEvent -SourceIdentifier $jobLogEventName -Action {
            Write-Host $event.MessageData
        }
    
        while ($installJob.State -eq "Running") {
            Start-Sleep -Milliseconds 200
        }
    } finally {
        Receive-Job $installJob
        Unregister-Event -SourceIdentifier $jobLogEventName -Force
        $installJob, $progressListener | ?{ $_ -ne $null } | Stop-Job -PassThru | Remove-Job 
    }
}

function Invoke-QuickType {
    [CmdletBinding(DefaultParameterSetName="default")]
    param(
        [Parameter(ParameterSetName="script", Mandatory=$true)]
        [string]$Script,
        [Parameter(ParameterSetName="show-scripts", Mandatory=$true)]
        [switch]$ShowScripts,
        
        [Parameter(ParameterSetName="default", Mandatory=$false)]
        [switch]$Project,
        [Parameter(ParameterSetName="default", Mandatory=$false)]
        [string]$ProjectName,
        [Parameter(ParameterSetName="default", Mandatory=$false)]
        [string]$Preset,
        [Parameter(ParameterSetName="default", Mandatory=$false)]
        [string]$Src,
        [Parameter(ParameterSetName="default", Mandatory=$false)]
        [string]$Out,
        [Parameter(ParameterSetName="script", Mandatory=$false, ValueFromRemainingArguments=$true)]
        [Parameter(ParameterSetName="default", Mandatory=$false, ValueFromRemainingArguments=$true)]
        [string[]]$Arguments
    )
    
    $nodePackagePath = Find-PackagePath -PackageId "Node.js.redist"
    if (-not $nodePackagePath) {
        Write-Error "Could not find the Node.js redistributable package. Install it with: 'Install-Package Node.js.redist'"
    }
    if (-not (Test-QuickTypeInstallation)) {
        Write-Error "The QuickType NPM package is not installed. Install it with: 'Install-QuickType'"
    }

    $dteProject = if ($ProjectName) { Get-Project -Name $ProjectName } else { Get-Project }
    $projectDir = Split-Path -Path $dteProject.FullName
    $willAddToVSProject = $Project.IsPresent -and $Src -and $Out
    $originalOut = $Out
    if ($willAddToVSProject) {
        if ([IO.Path]::IsPathRooted($Out)) {
            Write-Error "When using quicktype with the Project switch, the Out path must be relative."
        }
        $Out = Join-Path $projectDir $Out
    }

    $packageArchitecture = Get-PackageArchitecture -ToolsPath $ToolsPath -UseIsolation
    $nodePackagePath = Join-Path $SolutionDir (Join-Path $nodePackagePath "tools\$($packageArchitecture)")
    $actionType = $PSCmdlet.ParameterSetName
    
    Invoke-WithPathEnvironment -Paths $nodePackagePath -Action {
        try {
            $targetPath = if ($Src) { Split-Path -Path $Src } else { $QuickTypeInstallDir }
            Push-Location $targetPath
            
            $executableName = Join-Path $QuickTypeInstallDir "quicktype.cmd"
            if (-not (Get-Command $executableName)) {
                Write-Error "Could not find the QuickType executable, please re-install with: 'Install-QuickType -Force'"
            }

            switch ($actionType) {
                "script" {
                    Write-Host "Executing script '$($Script)'..."
                    & {
                        . (Join-Path $QuickTypeInstallDir $Script) @Arguments
                    }
                }
                "show-scripts" {
                    Write-Host "Enumerating available QuickType scripts:"
                    Get-ChildItem -Path $QuickTypeInstallDir -Filter "*.ps1" | %{
                        Write-Host "$([IO.Path]::GetFileNameWithoutExtension($_.Name))"
                    }
                }
                Default {
                    $exeArgs = Set-QuickTypeArgumentsWithPreset -Preset $Preset -Arguments $Arguments
                    if ($Src) {
                        Add-ArrayItems -Array $exeArgs -Items "--src", (Split-Path -Path $Src -Leaf)
                        if ($willAddToVSProject) {
                            $outNamespaceParts = (
                                $dteProject.Properties.Item("DefaultNamespace").Value,
                                (ConvertTo-PathCsNamespace -Path (Split-Path -Path $originalOut))
                            )
                            $outNamespace = ($outNamespaceParts | ?{ -not [String]::IsNullOrWhiteSpace($_) }) -join "."
                            Add-ArrayItems -Array $exeArgs -Items "--namespace", $outNamespace
                        }
                        if ($Out) {
                            Add-ArrayItems -Array $exeArgs -Items "--out", $Out
                            if ($willAddToVSProject) {
                                $outNameNoExt = [IO.Path]::GetFileNameWithoutExtension($Out)
                                Add-ArrayItems -Array $exeArgs `
                                    -Items "-t", (ConvertTo-ValidCsIdentifier -Identifier $outNameNoExt)
                            }
                        }
                    } elseif ($Preset -or $Out) {
                        Write-Error "[QuickType.PowerShell]: Reading from standard input is not supported."
                    } else {
                        $hasWhitelistedArgs = `
                            $Arguments.Length -eq 0 -or `
                            ($Arguments | ?{ $_ -match "^\s*--(help|src)\s*$" } | select -First 1) -ne $null
                        if (-not $hasWhitelistedArgs) {
                            Write-Error "[QuickType.PowerShell]: Some arguments are missing, please see: 'quicktype --help'"
                        }
                    }
    
                    & $executableName @exeArgs
                    if ($?) {
                        if ($Out) {
                            $postProcessInfo = Invoke-QuickTypePostProcess -OutPath $Out
                            if ($postProcessInfo.IsNormalized) {
                                Write-Host "The output file's line endings were normalized."
                            }
                        }
                        if ($willAddToVSProject) {
                            $dteProject.ProjectItems.AddFromFile($Out) | Out-Null
                            Write-Host "Added the output to project '$($dteProject.Name)'"
                        }
                    }
                }
            }
        } finally {
            Pop-Location
        }
    }
}

function Set-QuickTypeArgumentsWithPreset {
    param(
        [Parameter(Mandatory=$true)]
        [AllowEmptyCollection()]
        [AllowNull()]
        [string[]]$Arguments,
        [Parameter(Mandatory=$false)]
        [string]$Preset
    )
    
    $Arguments = if ($Arguments.Length -gt 0) { $Arguments } else { @() }
    $presetArguments = [Collections.ArrayList]@()
    switch ($Preset) {
        "schema2cs" { 
            Add-ArrayItems -Array $presetArguments `
                -Items "-s", "schema", "-l", "cs", "--csharp-version", "5", "--features", "attributes-only"
        }
    }
    if ($Arguments.Length -gt 0) {
        Add-ArrayItems -Array $presetArguments -Items "--telemetry", "disable"
    }

    Add-ArrayItems -Array $presetArguments -Items $Arguments
    return ,$presetArguments # Preserve the ArrayList's typing
}

function Invoke-QuickTypePostProcess {
    param (
        [Parameter(Mandatory=$true)][string]$OutPath
    )

    $postProcessJob = Start-Job -ScriptBlock {
        param($params)
        $ErrorActionPreference = "Stop"
        . (Join-Path $params.ToolsPath "common_init.ps1")

        Import-PathLibraries -Path $params.ToolsPath

        return [QuickType.PowerShell.Core.QuickTypePostProcessor]::Execute($params.OutPath)
    } -ArgumentList @{
        OutPath = $OutPath;
        ToolsPath = $ToolsPath;
    }
    $postProcessInfo = Wait-Job $postProcessJob | Receive-Job
    $postProcessJob | Stop-Job -PassThru | Remove-Job
    return $postProcessInfo
}

function Test-QuickTypeInstallation {
    return Test-Path -Path $VersioningFile -PathType Leaf
}

New-Alias -Name "quicktype" -Value Invoke-QuickType -Force

Export-ModuleMember -Function Install-QuickType
Export-ModuleMember -Function Invoke-QuickType -Alias "quicktype"
