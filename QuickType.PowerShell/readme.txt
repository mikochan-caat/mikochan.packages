﻿You have successfully installed the QuickType PowerShell package.

To begin, type the command in the Package Manager Console:
    Install-QuickType
Then, install the Node.js runtime from NuGet:
    Install-Package Node.js.redist

Once the NPM package is installed, you can now invoke quicktype in the Package Manager Console.

To start, you can execute quicktype targeting your Visual Studio project:
    quicktype -project [-projectname [name]] -src [path] -out [path] [[-argumentlist ][args...]]
    example:
        for project: $(SolutionDir)\MyProject.CSharp
        quicktype -project -src [path] -out "\Path\Relative\To\Project\file.cs" [[-argumentlist ][args...]]

A JSON Schema to C# preset is available using the following command:
    quicktype [-project] -src [path] -out [path] -preset schema2cs [[-argumentlist ][args...]]

You can also add custom QuickType PowerShell scripts, just place them in the following folder:
    $(SolutionDir)\.tools\quicktype.powershell\
    The following useful ambient variables are available:
    $ToolsPath
    $SolutionNode
    $SolutionDir
    $Package
    $QuickTypeInstallDir

...and to invoke them type:
    quicktype -script [script_name_without_extension]
    example:
        for script: $(SolutionDir)\.tools\quicktype.powershell\MyJsonSchema.ps1
        quicktype -script MyJsonSchema [args...]

...to check for all available scripts:
    quicktype -showscripts

For more information, type:
    quicktype --help
    Get-Help quicktype
