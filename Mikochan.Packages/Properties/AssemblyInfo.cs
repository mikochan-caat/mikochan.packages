﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Mikochan.Packages Helpers Library")]
[assembly: AssemblyDescription("Provides common managed helpers for packages in this product.")]
[assembly: AssemblyProduct("Mikochan.Packages")]
[assembly: AssemblyCopyright("Copyright © 2019 ITDCAT")]

[assembly: ComVisible(false)]
[assembly: Guid("c50414c2-b284-413a-b318-1af688eacf8b")]

[assembly: AssemblyVersion("1.0.*")]
