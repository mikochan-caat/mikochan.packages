﻿using System;
using System.Collections.Generic;
using PolyfillsForOldDotNet.System.Runtime.InteropServices;

namespace Mikochan.Packages.Core
{
    public sealed class OSInfo
    {
        public static readonly OSPlatform UnknownPlatform = OSPlatform.Create("Unknown");

        private static readonly Dictionary<OSPlatform, string> OSMapping =
            new Dictionary<OSPlatform, string> {
                { OSPlatform.Windows, "win" },
                { OSPlatform.Linux, "linux" },
                { OSPlatform.OSX, "osx" }
            };
        private static readonly Lazy<OSInfo> OSInstance = new Lazy<OSInfo>(CreateInstance);

        public static OSInfo Current
        {
            get { return OSInstance.Value; }
        }

        public OSPlatform Platform { get; private set; } 
        public string PlatformString { get; private set; }

        private OSInfo() { }

        private static OSInfo CreateInstance()
        {
            foreach (var pair in OSMapping) {
                if (RuntimeInformation.IsOSPlatform(pair.Key)) {
                    return new OSInfo {
                        Platform = pair.Key,
                        PlatformString = pair.Value
                    };
                }
            }
            return new OSInfo {
                Platform = UnknownPlatform,
                PlatformString = "unknown"
            };
        }
    }
}
