﻿using System;
using PolyfillsForOldDotNet.System.Runtime.InteropServices;

namespace Mikochan.Packages.Core
{
    public static class PackageArchitecture
    {
        public static string GetPackageString()
        {
            string packageString = String.Format(
                "{0}-{1}", 
                OSInfo.Current.PlatformString, 
                RuntimeInformation.OSArchitecture);
            return packageString.ToLowerInvariant();
        }
    }
}
