﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ionic.Zip;

namespace Mikochan.Packages.Core
{
    public static class ZipArchiveHandler
    {
        public static IEnumerable<int> GetExtractionEnumerator(string filePath, string targetDir)
        {
            if (filePath == null) {
                throw new ArgumentNullException("filePath");
            }
            if (targetDir == null) {
                throw new ArgumentNullException("targetDir");
            }
            if (!ZipFile.IsZipFile(filePath)) {
                string errorMessage = String.Format(
                    @"The file at '{0}' is not a valid Zip file or is damaged.",
                    filePath);
                throw new ArgumentException(errorMessage);
            }

            using (var zipFile = ZipFile.Read(filePath)) {
                return ExtractCore(zipFile, targetDir).Distinct();
            }
        }

        private static IEnumerable<int> ExtractCore(ZipFile zipFile, string targetDir)
        {
            int extractedCount = 0;
            foreach (var file in zipFile) {
                file.Extract(targetDir, ExtractExistingFileAction.OverwriteSilently);
                yield return (int)((++extractedCount / (double)zipFile.Count) * 100);
            }
        }
    }
}
