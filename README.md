# Miko-chan's NuGet Package Repository

This repository contains various personal and modified NuGet packages for use in my own projects.  
All tools are built to support .NET 4.0 and Visual Studio 2010.

## Modified Packages

### Linq2Db.SqlServer

Normally a project-level package, this version of Linq2Db.SqlServer is now a solution-level package.  
Upon installation, no template files will be immediately installed.

To install the templates, select the target project in the NuGet Package Manager console and then execute:

```[powershell]
Install-Linq2DbTemplates [-ProjectSubPath ""]
```

This package also has an optimized ```init.ps1``` that only copies the solution Linq2Db dependencies
if they don't exist or are a different version.

### T4MVC

Normally a project-level package, this version of T4MVC is now a solution-level package.  
Upon installation, no template files will be immediately installed.

To install the templates, select the target project in the NuGet Package Manager console and then execute:

```[powershell]
Install-T4MVC [-ProjectSubPath ""] [-TemplateType ""]
```

This package also contains predefined T4MVC templates under ```[Package]\tools\content\```.  
To select those predefined templates instead of the default one, use the ```TemplateType``` parameter
in ```Install-T4MVC```.

```[powershell]
Install-T4MVC -ProjectSubPath "MyProject\Templates" -TemplateType "miko.html5"
```

## Custom Packages

### Reegenerator

This package bootstraps a renderers library project created by [Reegenerator](http://www.reegenerator.com/?context=toolsmenu) 
and will add additional reference assemblies (e.g. ```EnvDTE```, ```VSLangProj```), 
configure the ```EmbedInteropTypes``` options for certain assemblies, and add additional build targets to ensure
the proper running of the Reegenerator VS extension.

### MVC JS and CSS Toolchain

This package provides MsBuild compilation targets for TypeScript and SASS files. This package also provides a
JSON settings file for SASS compilation (for TypeScript, a location to a ```tsconfig.json``` file is provided) 
and a PowerShell cmdlet (accessible from the NuGet Package Manager console) to open up an instance of VSCode 
with the TypeScript and SASS compilers set to ```watch``` mode on startup.

**NOTE:** This package is recommended for users of VS 2010 only. For 2012+, use something like [WebCompiler](https://marketplace.visualstudio.com/items?itemName=MadsKristensen.WebCompiler) and 
[TypeScript for Visual Studio](https://www.microsoft.com/en-us/download/details.aspx?id=48593).

### QuickType.PowerShell

This solution-level package provides a PowerShell cmdlet wrapper around the [quicktype](https://github.com/quicktype/quicktype) 
Node.js CLI and provides a way for ```quicktype``` to interact with the current ```DTE``` object.
This cmdlet can be accessed from the NuGet Package Manager console.

For more information, check the package's ```readme.txt```.

### Workspace Toolbox

This solution-level package provides a set of utility PowerShell cmdlets that can be used from the 
NuGet Package Manager console.  
Cmdlet categories include: ```ASP.NET Deployment to IIS/Express```, ```NuGet Package Restore Script Caller```, 
```Solution/Workspace Utilities```.

**NOTE:** The deployment cmdlets are very personalized for my needs, they are very likely to be not useful for you as is.
