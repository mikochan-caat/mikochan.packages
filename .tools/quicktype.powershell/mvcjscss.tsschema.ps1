quicktype -preset schema2cs `
    -src "..\..\Mvc.JsCss.Toolchain\Core\Schemas\TypeScriptConfig\tsconfig.schema.json" `
    -out "Core\Schemas\TypeScriptConfig\TypeScriptConfig.cs" `
    -project `
    -projectname "Mvc.JsCss.Toolchain"
