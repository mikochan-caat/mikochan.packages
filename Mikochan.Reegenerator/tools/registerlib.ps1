& {
    $libraryName = "Kodeo.Reegenerator.dll"
    $reegeneratorExtensionPath = `
        [AppDomain]::CurrentDomain.GetAssemblies() |
        ?{ $_.Location -match "^.*\\$($libraryName)$" } |
        Select-Object -ExpandProperty Location -First 1

    if ($reegeneratorExtensionPath) {
        $extensionAssembly = [Reflection.Assembly]::ReflectionOnlyLoadFrom($reegeneratorExtensionPath)
        $sourceLibPath = $null
        if ($extensionAssembly.GetName().GetPublicKeyToken().Length) {
            $sourceLibPath = Join-Path $ToolsPath "lib\sl\$($libraryName)"
        } else {
            $sourceLibPath = Join-Path $ToolsPath "lib\ul\$($libraryName)"
        }
        $targetLibPath = Join-Path $InstallPath "lib\net40\$($libraryName)"
        $sourceLength = Get-ChildItem -Path $sourceLibPath | Select-Object -ExpandProperty Length
        $targetLength = Get-ChildItem -Path $targetLibPath | Select-Object -ExpandProperty Length
        if ($sourceLength -ne $targetLength) {
            Copy-Item -Path $sourceLibPath -Destination $targetLibPath -Force | Out-Null
        }
    }
}
