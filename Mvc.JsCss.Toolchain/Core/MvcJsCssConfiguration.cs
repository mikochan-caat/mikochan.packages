﻿using System.IO;
using System.Threading.Tasks;
using Mvc.JsCss.Toolchain.Core.Schemas.BuildConfiguration;
using Newtonsoft.Json.Linq;
using NJsonSchema;

namespace Mvc.JsCss.Toolchain.Core
{
    public sealed class MvcJsCssConfiguration : IMvcJsCssConfiguration
    {
        public string TS_ProjectFilePath { get; set; }
        public string TS_AdditionalArgs { get; set; }
        public string SASS_SourcePath { get; set; }
        public string SASS_DestinationPath { get; set; }
        public string SASS_AdditionalArgs { get; set; }

        private MvcJsCssConfiguration() { }

        public static MvcJsCssConfiguration ReadFromFile(string configPath, string schemaPath)
        {
            var configuration = new MvcJsCssConfiguration();
            MapFromFile(configPath, schemaPath, configuration);
            return configuration;
        }

        public static void MapFromFile(string configPath, string schemaPath, IMvcJsCssConfiguration target)
        {
            string jsonConfigString = File.ReadAllText(configPath);
            var configSchema = TaskEx.Run(() => JsonSchema4.FromFileAsync(schemaPath))
                .GetAwaiter()
                .GetResult();
            var validationErrors = configSchema.Validate(jsonConfigString);
            if (validationErrors.Count > 0) {
                throw new MvcJsCssConfigurationException(configPath, validationErrors);
            }

            // General
            var parsedConfig = JObject.Parse(jsonConfigString, new JsonLoadSettings {
                CommentHandling = CommentHandling.Ignore
            });
            var config = parsedConfig.ToObject<BuildConfiguration>();

            // TypeScript
            var typeScriptConfig = config.TypeScript;
            target.TS_ProjectFilePath = typeScriptConfig.ProjectFilePath;
            target.TS_AdditionalArgs = typeScriptConfig.AdditionalArgs;

            // SASS
            var sassConfig = config.Sass;
            target.SASS_SourcePath = sassConfig.SourcePath;
            target.SASS_DestinationPath = sassConfig.DestinationPath;
            target.SASS_AdditionalArgs = sassConfig.AdditionalArgs;
        }
    }
}
