﻿using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using Newtonsoft.Json;

namespace Mvc.JsCss.Toolchain.Core.Tasks
{
    public sealed class ReadMvcJsCssConfigurationTask : Task, IMvcJsCssConfiguration
    {
        [Required]
        public string ConfigurationFilePath { get; set; }
        [Required]
        public string ConfigurationSchemaPath { get; set; }

        [Output]
        public string TS_ProjectFilePath { get; set; }
        [Output]
        public string TS_AdditionalArgs { get; set; }
        [Output]
        public string SASS_SourcePath { get; set; }
        [Output]
        public string SASS_DestinationPath { get; set; }
        [Output]
        public string SASS_AdditionalArgs { get; set; }

        public override bool Execute()
        {
            try {
                MvcJsCssConfiguration.MapFromFile(
                    this.ConfigurationFilePath,
                    this.ConfigurationSchemaPath,
                    this);
            } catch (MvcJsCssConfigurationException ex) {
                this.Log.LogErrorFromException(ex, false, false, this.ConfigurationFilePath);
                return false;
            }
            return true;
        }
    }
}
