﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using Mvc.JsCss.Toolchain.Core.Schemas.TypeScriptConfig;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Mvc.JsCss.Toolchain.Core.Tasks
{
    public sealed class ReadTypeScriptProjectConfigTask : Task
    {
        private const string TypeScriptProjectFileName = "tsconfig.json";
        private const string TypeScriptPackageName = "Microsoft.TypeScript.Compiler";
        private const string TypeScriptPackageNameWildcard = TypeScriptPackageName + "*";
        private const string TypeScriptPackageNameRegex =
            "^" + TypeScriptPackageName + @"(\.\d+){3,}(-.+)?$";

        [Required]
        public string ProjectFilePath { get; set; }
        [Required]
        public string TypeScriptPackageSearchPath { get; set; }

        [Output]
        public string Options_OutDir { get; set; }
        [Output]
        public string TypeScriptCompilerPath { get; set; }

        public override bool Execute()
        {
            string tsConfigFile = Path.Combine(this.ProjectFilePath, TypeScriptProjectFileName);
            TypeScriptConfig tsConfig;
            try {
                tsConfig = this.GetTypeScriptProjectConfig(tsConfigFile);
            } catch(Exception ex) {
                this.Log.LogErrorFromException(ex, true, true, tsConfigFile);
                return false;
            }

            this.Options_OutDir = tsConfig.CompilerOptions.OutDir;
            this.TypeScriptCompilerPath = this.FindTypeScriptCompiler();

            return true;
        }

        private TypeScriptConfig GetTypeScriptProjectConfig(string tsConfigFile)
        {
            string tsConfigJson = File.ReadAllText(tsConfigFile);
            var parsedConfig = JObject.Parse(tsConfigJson, new JsonLoadSettings {
                CommentHandling = CommentHandling.Ignore
            });
            return parsedConfig.ToObject<TypeScriptConfig>();
        }

        private string FindTypeScriptCompiler()
        {
            var dirEnumerator = Directory.EnumerateDirectories(
                this.TypeScriptPackageSearchPath,
                TypeScriptPackageNameWildcard, 
                SearchOption.TopDirectoryOnly);
            string path = dirEnumerator
                .Select(dir => new DirectoryInfo(dir))
                .Where(this.TestTypeScriptPackageName)
                .OrderByDescending(dir => dir.Name)
                .Select(dir => dir.FullName)
                .FirstOrDefault();
            if (path == null) {
                throw new DirectoryNotFoundException(
                    "Could not find an installed TypeScript compiler package.");
            }
            return Path.Combine(path, @"tools\");
        }

        private bool TestTypeScriptPackageName(DirectoryInfo dir)
        {
            return Regex.IsMatch(dir.Name, TypeScriptPackageNameRegex, RegexOptions.IgnoreCase);
        }
    }
}
