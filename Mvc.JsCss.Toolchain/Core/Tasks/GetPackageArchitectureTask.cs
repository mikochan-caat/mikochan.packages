﻿using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using Mikochan.Packages.Core;

namespace Mvc.JsCss.Toolchain.Core.Tasks
{
    public sealed class GetPackageArchitectureTask : Task
    {
        [Output]
        public string PackageString { get; set; }

        public override bool Execute()
        {
            this.PackageString = PackageArchitecture.GetPackageString();
            return true;
        }
    }
}
