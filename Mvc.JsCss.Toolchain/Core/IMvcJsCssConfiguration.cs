﻿namespace Mvc.JsCss.Toolchain.Core
{
    public interface IMvcJsCssConfiguration
    {
        string TS_ProjectFilePath { get; set; }
        string TS_AdditionalArgs { get; set; }
        string SASS_SourcePath { get; set; }
        string SASS_DestinationPath { get; set; }
        string SASS_AdditionalArgs { get; set; }
    }
}
