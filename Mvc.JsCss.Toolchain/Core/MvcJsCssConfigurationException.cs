﻿using System;
using System.Collections.Generic;
using System.Linq;
using NJsonSchema.Validation;

namespace Mvc.JsCss.Toolchain.Core
{
    public sealed class MvcJsCssConfigurationException : Exception
    {
        public MvcJsCssConfigurationException(
            string configPath, IEnumerable<ValidationError> validationErrors)
            : base(CreateExceptionMessage(configPath, validationErrors))
        {
        }

        private static string CreateExceptionMessage(
            string configPath, IEnumerable<ValidationError> validationErrors)
        {
            validationErrors = validationErrors ?? Enumerable.Empty<ValidationError>();
            if (!validationErrors.Any()) {
                return "An error occurred while reading the JSON configuration file.";
            }

            var prependedStrings = new string[] { 
                "The JSON configuration file contains validation errors:",
                "@" + configPath
            };
            var errorMessages = validationErrors.Select(FormatValidationError);
            return String.Join(Environment.NewLine, prependedStrings.Concat(errorMessages));
        }

        private static string FormatValidationError(ValidationError error)
        {
            if (error.HasLineInfo) {
                return String.Format(
                    "{0}: {1} at line {2} pos {3}.",
                    error.Kind,
                    error.Path,
                    error.LineNumber,
                    error.LinePosition);
            }
            return error.ToString();
        }
    }
}
