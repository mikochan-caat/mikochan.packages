$ErrorActionPreference = "Stop"

$action = $args[0]

$sassExecutable = Join-Path $env:MVCJSCSS_SASS_COMPILER_PATH "sass.bat"

if (-not (Test-Path -Path $sassExecutable)) {
    Write-Error "Could not find the SASS compiler."
}

$escapedSourcePath = "`"$($env:MVCJSCSS_SASS_SOURCE_PATH)`"/"
$escapedDestinationPath = "`"$($env:MVCJSCSS_SASS_DESTINATION_PATH)`"/"
$additionalArgs = $env:MVCJSCSS_SASS_ADDITIONAL_ARGS

$showSuccessMessage = $false
$watchOption = ""
switch ($action) {
    "build" {
        $showSuccessMessage = $true
    }
    "watch" {
        $watchOption = "--watch"
    }
    Default {
        Write-Error "Unknown action '$($action)'."
    }
}

& $sassExecutable `
    "$($escapedSourcePath):$($escapedDestinationPath)" `
    --update --embed-sources --embed-source-map $watchOption $additionalArgs
if ($? -and $showSuccessMessage) {
    Write-Host "SASS source files were successfully built."
}
exit (-not $?)
