$ErrorActionPreference = "Stop"

$action = $args[0]

$nodeToolsPath = [IO.Path]::GetFullPath("$($env:MVCJSCSS_NODEJS_PATH)")
$typeScriptToolsPath = [IO.Path]::GetFullPath("$($env:MVCJSCSS_TS_COMPILER_PATH)")

$nodeExecutable = Join-Path $nodeToolsPath "node.exe"
$typeScriptExecutable = Join-Path $typeScriptToolsPath "tsc.exe"
$typeScriptNodeExecutable = Join-Path $typeScriptToolsPath "tsc.js"

if (-not (Test-Path -Path $typeScriptExecutable)) {
    Write-Error "Could not find the TypeScript compiler."
}
if (-not (Test-Path -Path $nodeExecutable)) {
    Write-Error "Could not find the Node.js runtime."
}
if (-not (Test-Path -Path $typeScriptNodeExecutable)) {
    Write-Error "Could not find the TypeScript Node.js compiler."
}

$projectFile = [IO.Path]::GetFullPath("$(Join-Path $env:MVCJSCSS_TS_PROJECTFILE_PATH tsconfig.json)")
$escapedProjectFile = "`"$($projectFile)`""
$additionalArgs = $env:MVCJSCSS_TS_ADDITIONAL_ARGS
switch ($action) {
    "build" {
        & $typeScriptExecutable --build $additionalArgs $escapedProjectFile
        if ($?) {
            Write-Host "TypeScript project was successfully built."
        }
    }
    "watch" {
        & $nodeExecutable "`"$($typeScriptNodeExecutable)`"" --build --watch $additionalArgs $escapedProjectFile
    }
    Default {
        Write-Error "Unknown action '$($action)'."
    }
}
exit (-not $?)
