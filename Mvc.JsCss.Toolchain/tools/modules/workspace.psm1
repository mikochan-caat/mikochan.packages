. (Join-Path $PSScriptRoot "..\module_init.ps1") @args

function Start-MvcJsCssCode {
    $project = Get-Project
    $projectDir = Split-Path $project.FullName
    $nodePackagePath = Join-Path $SolutionDir (Find-PackagePath -PackageId "Node.js.redist")
    $typeScriptPackagePath = Join-Path $SolutionDir (Find-PackagePath -PackageId "Microsoft.TypeScript.Compiler")

    $launchJob = Start-Job -ScriptBlock {
        param($params)
        $ErrorActionPreference = "Stop"
        . (Join-Path $params.ToolsPath "common_init.ps1")

        Import-PathLibraries -Path $params.ToolsPath

        $configFile = Join-Path $params.ProjectDir "Mvc.JsCss.build.json"
        $schemaFile = Join-Path $params.ProjectDir "Mvc.JsCss.build.schema.json"
        $config = [Mvc.JsCss.Toolchain.Core.MvcJsCssConfiguration]::ReadFromFile($configFile, $schemaFile)
        $packageArchitecture = [Mikochan.Packages.Core.PackageArchitecture]::GetPackageString()

        # General Environment Variables
        $env:MVCJSCSS_IS_DEFINED = $true
        $env:MVCJSCSS_COMPILERS_PATH = Join-Path $params.SolutionDir ".tools\mvcjscss.compilers\"
        $env:MVCJSCSS_NODEJS_PATH = Join-Path $params.NodePackagePath "tools\$($packageArchitecture)"
    
        # TypeScript Environment Variables
        $env:MVCJSCSS_TS_COMPILER_PATH = Join-Path $params.TypeScriptPackagePath "tools"
        $env:MVCJSCSS_TS_PROJECTFILE_PATH = $config.TS_ProjectFilePath
        $env:MVCJSCSS_TS_ADDITIONAL_ARGS = $config.TS_AdditionalArgs
    
        # SASS Environment Variables
        $env:MVCJSCSS_SASS_COMPILER_PATH = Join-Path $params.ToolsPath "dart-sass\$($packageArchitecture)"
        $env:MVCJSCSS_SASS_SOURCE_PATH = $config.SASS_SourcePath
        $env:MVCJSCSS_SASS_DESTINATION_PATH = $config.SASS_DestinationPath
        $env:MVCJSCSS_SASS_ADDITIONALARGS = $config.SASS_AdditionalArgs
    
        code.cmd -n "`"$($params.ProjectDir)`"" | Out-Null
    } -ArgumentList @{
        ProjectDir = $projectDir;
        SolutionDir = $SolutionDir;
        ToolsPath = $ToolsPath;
        NodePackagePath = $nodePackagePath;
        TypeScriptPackagePath = $typeScriptPackagePath;
    }
}

New-Alias -Name "mvccode" -Value Start-MvcJsCssCode -Force

Export-ModuleMember -Function Start-MvcJsCssCode -Alias "mvccode"
