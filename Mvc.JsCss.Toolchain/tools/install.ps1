#Requires -version 3.0
param($InstallPath, $ToolsPath, $Package, $Project)
$ErrorActionPreference = 'Stop'

$projectPath = Split-Path $Project.FileName
$toolsContentPath = Join-Path $ToolsPath "content"
Get-ChildItem $toolsContentPath | %{
    Copy-Item -Path $_.FullName -Destination $projectPath -Recurse -Force
}
