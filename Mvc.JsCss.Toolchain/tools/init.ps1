#Requires -version 3.0
param($InstallPath, $ToolsPath, $Package)
$ErrorActionPreference = 'Stop'

. (Join-Path $PSScriptRoot "module_importer.ps1")

Register-Module "workspace.psm1"

# Copy the compiler scripts (if they do not exist or are older)
$solutionRoot = Split-Path $dte.Solution.FileName
$compilerScriptsName = "mvcjscss.compilers"
$compilerScriptsPath = Join-Path $ToolsPath $compilerScriptsName
$targetPath = Join-Path $solutionRoot ".tools\$($compilerScriptsName)"

. (Join-Path $PSScriptRoot "robocopy.ps1")
Copy-DirectoryMirroredAsync -Source $compilerScriptsPath -Destination $targetPath -Filter "*.ps1"
