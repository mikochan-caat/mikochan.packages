#Requires -version 3.0
param($InstallPath, $ToolsPath, $Package)
$ErrorActionPreference = 'Stop'

. (Join-Path $PSScriptRoot "module_importer.ps1")

Register-Module "deploy.psm1"
Register-Module "nuget.psm1"
Register-Module "workspace.psm1"

# Clean all accumulated paths in the PATH environment variable for this VS instance
$env:PATH = ($env:PATH -split ';' | Sort-Object -Unique) -join ';'
