﻿$SiteIisAppName = "OneAccomplishment"
$SolutionDeploymentFolder = ".deployment"
$SolutionLocalDeploymentFolder = Join-Path $SolutionDeploymentFolder ".local"
$SolutionLocalDeploymentTempFolder = Join-Path $SolutionDeploymentFolder ".local_temp"
$DeploymentPermissionsCsvPath = Join-Path $SolutionDeploymentFolder "iisapp.permissions.csv"
$LocalDeploymentOptionsPath = Join-Path $SolutionDeploymentFolder "local.options.json"

function Get-SitePath {
    try {
        return Get-WebFilePath "IIS:\Sites\$($SiteIisAppName)" | Select-Object -ExpandProperty FullName
    } catch {
        return $null
    }
}
