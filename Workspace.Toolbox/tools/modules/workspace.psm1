. (Join-Path $PSScriptRoot "..\module_init.ps1") @args

function Reset-Solution {
    param (
        [switch]$Save
    )
    if ($Save.IsPresent -and -not $SolutionNode.Saved) {
        $SolutionNode.SaveAs($SolutionNode.FullName);
    }
    (Get-Item $SolutionNode.FullName).LastWriteTime = Get-Date
}

New-Alias -Name "ldsln" -Value Reset-Solution -Force

Export-ModuleMember -Function Reset-Solution -Alias "ldsln"
