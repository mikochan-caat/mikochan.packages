. (Join-Path $PSScriptRoot "..\module_init.ps1") @args

function Invoke-PostRestore {
    param (
        [string]$ScriptDir
    )

    $fileName = "ngpres.ps1"
    $scriptFile = if ($ScriptDir) { Join-Path $ScriptDir $fileName } else { $fileName }
    $scriptPath = Join-Path $SolutionDir $scriptFile
    if (-not (Test-Path -Path $scriptPath)) {
        Write-Host "No script for NuGet post-restoration was found. Create one with the name '$($fileName)'."
        return
    }

    Write-Host "Executing NuGet post-restoration script '$($fileName)'..."
    & {
        . $scriptPath
    }
}

New-Alias -Name "ngpres" -Value Invoke-PostRestore -Force

Export-ModuleMember -Function Invoke-PostRestore -Alias "ngpres"
