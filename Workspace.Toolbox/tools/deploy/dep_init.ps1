﻿. (Join-Path $args[0] "common_init.ps1")
. (Join-Path $args[0] "deploy_init.ps1")

# Shared vars initialization
$ToolsPath = $args[0]
$CurrentLocation = $args[1]
$CurrentConfig = $args[2]
$SolutionPath = $args[3]
$SolutionDir = Split-Path $SolutionPath

$SiteProjectName = "OneAccomplishment.Site"
$SiteProjectPath = Join-Path $SolutionDir $SiteProjectName
$SiteProjectFile = Join-Path $SiteProjectPath "$($SiteProjectName).csproj"
$SiteProjectPackagePath = Join-Path $SiteProjectPath "obj\$($CurrentConfig)\Package"

Set-Location $CurrentLocation

function Start-Deployment {
    param (
        [Parameter(Mandatory=$true)][scriptblock]$Continuation
    )
    Write-Notice "Build configuration is '$($CurrentConfig)'."
    Write-Host ""

    # Set build tools path
    $vsCommonToolsPath = $env:VS100COMNTOOLS
    cmd.exe /c "call `"$($vsCommonToolsPath)\vsvars32.bat`" > nul 2>&1 && set" | .{process{
        if ($_ -match '^([^=]+)=(.*)') {
            [System.Environment]::SetEnvironmentVariable($matches[1], $matches[2])
        }
    }}
    if (-not $?) {
        Write-Critical "Visual Studio environment variables could not be set."
        Wait-Exit
    }

    # Build the project
    Write-Notice "Building the website and its project dependencies..."
    msbuild.exe "$($SiteProjectFile)" /t:Package /p:Configuration=$CurrentConfig /p:AutoParameterizationWebConfigConnectionStrings=false
    if (-not $?) {
        Write-Critical "Build failed or was cancelled."
        Wait-Exit
    }

    # Get the Web Deploy installation path and executable
    Write-Host ""
    Write-Notice "Performing deployment..."
    $webDeployInstallPath = Get-ItemProperty "HKLM:\SOFTWARE\Microsoft\IIS Extensions\MSDeploy\*" | `
                            Sort-Object -Descending -Property PSPath | `
                            Select-Object -ExpandProperty InstallPath -First 1
    if (-not $webDeployInstallPath) {
        Write-Critical "Could not find an installation of MS Web Deploy on this system."
        Wait-Exit
    }
    $WebDeployExe = Join-Path $webDeployInstallPath "msdeploy.exe"

    Invoke-Command -ScriptBlock $Continuation
}
