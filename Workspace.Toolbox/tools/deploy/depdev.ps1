﻿. (Join-Path $PSScriptRoot "dep_init.ps1") @args

Start-Deployment {
    Write-Notice "Deploying to local file system folder..."

    $localDeployPath = Join-Path $SolutionDir $SolutionLocalDeploymentFolder
    New-Item -ItemType Directory -Path $localDeployPath -Force | Out-Null
    
    $env:SiteProjectDeployFilesPath = "`"$(Join-Path $SiteProjectPackagePath "PackageTmp")`""
    $env:LocalDeployPath = "`"$($localDeployPath)`""
    & $WebDeployExe --% -verb:sync -source:dirPath=%SiteProjectDeployFilesPath% -dest:dirPath=%LocalDeployPath%
    if (-not $?) {
        Write-Critical "Deployment failed or was cancelled."
        Wait-Exit
    }
    
    Write-Host ""
    Write-Notice "Deployment completed."
    Wait-Pause
}
