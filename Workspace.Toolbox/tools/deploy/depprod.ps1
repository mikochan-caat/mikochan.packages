﻿. (Join-Path $PSScriptRoot "dep_init.ps1") @args

Start-Deployment {
    Write-Notice "Deploying to IIS Website Application..."

    $siteProjectDeployPackagePath = Join-Path $SiteProjectPackagePath "$($SiteProjectName).zip"
    $env:WebDeployArgs = (
        "-verb:sync",
        "-source:package=`"$($siteProjectDeployPackagePath)`"",
        "-dest:auto,computerName=`"localhost`",includeAcls=`"False`"",
        "-disableLink:AppPoolExtension -disableLink:ContentExtension -disableLink:CertificateExtension",
        "-setParam:name=`"IIS Web Application Name`",value=`"$($SiteIisAppName)`""
    ) -join " "
    & $WebDeployExe --% %WebDeployArgs%
    if (-not $?) {
        Write-Critical "Deployment failed or was cancelled."
        Wait-Exit
    }

    $iisAppPath = Get-SitePath
    if (-not $iisAppPath) {
        Write-Critical "Could not find an IIS Website Application with the name '$($SiteIisAppName)'."
        Wait-Exit
    }

    # Clear all ACLs of target folder
    Write-Host ""
    Write-Notice "Clearing site ACLs..."
    $acl = Get-Acl $iisAppPath
    $acl.SetAccessRuleProtection($true, $true)
    $acl.Access | %{ $acl.RemoveAccessRule($_) | Out-Null }

    # Add built-in Administrators account
    $adminPermissions  = "BUILTIN\Administrators", "FullControl", "ObjectInherit", "None", "Allow"
    $adminRule = New-Object System.Security.AccessControl.FileSystemAccessRule $adminPermissions
    $acl.SetAccessRule($adminRule)
    Set-Acl $iisAppPath $acl

    # Import ACLs from CSV file
    Write-Notice "Importing site ACLs..."
    $csvPath = Join-Path $SolutionDir $DeploymentPermissionsCsvPath
    $permissions = Import-Csv -Path $csvPath
    foreach ($rule in $permissions) { 
        $path= $rule.Path
        $IdentityReference = $rule.IdentityReference
        $AccessControlType = $rule.AccessControlType
        $InheritanceFlags = $rule.InheritanceFlags
        $PropagationFlags = $rule.PropagationFlags
        $FileSystemRights = $rule.FileSystemRights
        $acl = Get-Acl $path
        $permission = @($IdentityReference), @($FileSystemRights), @($InheritanceFlags), @($PropagationFlags), @($AccessControlType)
        $accessRule = New-Object System.Security.AccessControl.FileSystemAccessRule $permission
        $acl.SetAccessRule($accessRule)
        $acl | Set-Acl $path
    }

    # Propagate ACLs to subfolders and files
    $currentUser = "$($env:USERDOMAIN)\$($env:USERNAME)"
    icacls.exe "$($iisAppPath)" /grant "$($currentUser):(OI)(CI)(M)"

    Write-Host ""
    Write-Notice "Deployment completed."
    Wait-Pause
}
